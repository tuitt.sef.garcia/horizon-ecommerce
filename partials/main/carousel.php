<?php
  /* Connect to DB */
  require "../controllers/connection.php";

  /* Query From Slides Table */
  $get_slides = "SELECT * FROM slides";

  /* Run the Query */
  $run_slides = mysqli_query($conn, $get_slides);

  /* Declare an empty slides array */
  $slides = [];

  /* Fetch all rows and add it to the slides array */
  while($row = mysqli_fetch_assoc($run_slides)) {
    $slides[] = $row;
  }
?>

<div class="bd-example">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      
      <?php foreach($slides as $key => $slide): ?>
        <div class="carousel-item <?php if($key == 0) { echo 'active' } ?>">
          <div class="overlay"></div>
          <img title="Image from Pexels" src="<?php echo $slide['slide_image'] ?>" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5> <?php echo $slide['slide_name'] ?> </h5>
            <p> <?php echo $slide['slide_description'] ?> </p>
          </div>
        </div>
      <?php endforeach; ?>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>