<footer class="bg-dark py-5 mt-5">
	<div class="container">
		<div class="float-right mb-0">
			<a href="#"> Back To Top </a>
		</div>
		<p class="mb-0 text-white ">
			&copy; <?php echo date("Y"); ?> <?php get_site_title(); ?>
		</p>
	</div>
</footer>